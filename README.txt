
-- SUMMARY --

The photo layover module lets users add overlay captions to their photos. The captions
are by default hidden, but on mouseover they slide into view.

-- USAGE --

There are two ways to use this module.
 1. As a filter on a node content type
 2. By manually calling theme('photo_layover')

Filter:
The filter replaces the theme on the flickr filter that comes with the flickr module and also adds another [photo-layover] filter.

theme('photo_layover', ...):
To manually call the theme function from another module or theme use the following parameters:
  theme('photo_layover', $img, $info, $options);

See photo-layover.tpl.php for the full range and description of all of the $info and $option parameters.

-- CONTACT --

For bug reports, feature suggestions and latest developments visit the
project page: http://drupal.org/project/photo_layover

Current maintainer:
 * Scott Hadfield (hadsie) <hadsie@gmail.com>
