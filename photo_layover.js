
/**
 * A simple behaviour to show/hide the captions
 */
Drupal.behaviors.photo_layover = function (context) {
  $(".photo-layover-wrapper", context).hover(
    function () {
      $(this).find('.photo-layover-info-box:not(.nohide)').slideDown("fast");
    },
    function () {
      $(this).find('.photo-layover-info-box:not(.nohide)').slideUp("fast");
    }
  );
}
