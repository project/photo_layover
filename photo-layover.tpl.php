<?php
/**
 * @file photo-layer.tpl.php
 *
 * Theme implementation for the photo layover.
 *
 * Available variables:
 * - $img: The html for the image to be displayed
 * - $info_top: The rendered content to be displayed in the top portion of
 *   the layover.
 * - $info_bottom: The rendered content to be displayed in the bottom portion of
 *   the layover.
 * - $width: The width of the photo.
 * - $height: The height of the photo.
 * - $nohidetop: TRUE if the top part should /not/ be hidden.
 * - $nohidebottom: TRUE if the bottom part should /not/ be hidden.
 *
 * Other variables:
 * - $info: The info array that contains all the data to be displayed in the
 *          caption (rendered results are $info_top and $info_bottom).
 *  - title: The title of the image.
 *  - link: A url to wrap around the title.
 *  - author: The author of the image.
 *  - author_url: A url to wrap around the author.
 *  - taken: The date the photo was taken.
 *  - posted: The date the photo was posted.
 *  - description: The description of the photo
 *
 * $options: The options array used to render $info.
 *  - width: The width of the photo (default: '100%').
 *  - height: The height of the photo (default: '100%').
 *  - position: The default layover to put content into (default: 'top').
 *  - $info-position: The layover to display this particular info
 *    field in (default: 'top').
 *  - labels: TRUE to show labels, FALSE to hide them (default: TRUE).
 *  - $info-label: Control an individual fields label (default: TRUE).
 *  - autohide: Whether or not to autohide the layover. If autohide is turned off
 *    then the layover will always be displayed over the photo (default: TRUE).
 *  - autohide-top: Autohide for the top layover (default: TRUE)
 *  - autohide-bottom: Autohide for the bottom layover (default: TRUE)
 *
 * @see template_preprocess_photo_layover()
 */
?>
<span class="photo-layover-wrapper" style="width: <?php print $width; ?>; height: <?php print $height; ?>;">
  <?php print $img; ?>
  
  <?php if ($info_top): ?>
   <span class="photo-layover-info-box photo-layover-info-box-top<?php if ($nohidetop) print ' nohide'; ?>">
      <span class="photo-layover-info"><?php print $info_top; ?></span>
    </span>
  <?php endif; ?>
  
  <?php if ($info_bottom): ?>
    <span class="photo-layover-info-box photo-layover-info-box-bottom<?php if ($nohidebottom) print ' nohide'; ?>">
      <span class="photo-layover-info"><?php print $info_bottom; ?></span>
    </span>
  <?php endif; ?>
</span>
  
